# OpenGL Lego Racer

A lego racer game written in C++ and OpenGL with dynamic cars.

## Usage

Use Visual Studio to open the project (`legoracer.sln`), then compile and run the game.

## OGLPack

The pack binaries are required for the project to build successfully.
